# Getting Started with the small frontend for the Challenge API

## Prerequisites

Before running the project, make sure the following software components are installed on your system:

- Node.js (at least version 12.0.0)
- npm (Node Package Manager)

## Installation

1. Clone the repository to your local machine.

   ```bash
   git clone https://github.com/your-username/your-project.git
   ```

2. Navigate to the project directory.

   ```bash
   cd your-project
   ```

3. Install the required dependencies.
   ```bash
   npm install
   ```

## Execution

After installing the project, run the following command to start the application in development mode:

```bash
npm start
```

The application will be compiled and opened in the browser at localhost:3000. If port 3000 is already in use, you can specify a different port by setting the PORT environment variable:

```bash
PORT=4000 npm start
```
