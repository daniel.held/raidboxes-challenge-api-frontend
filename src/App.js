import { useContext } from 'react';
import { BrowserRouter as Router, Route, Link, Routes } from 'react-router-dom';
import Register from './components/Register';
import Login from './components/Login';
import Logout from './components/Logout';
import Dashboard from "./components/Dashboard";

function App() {
    return (
        <Router>
            <div className="bg-gray-100">

                <Routes>
                    <Route path="/:method?/:id?" element={<Dashboard />} />
                    <Route path="/register" element={<Register />} />
                    <Route path="/logout" element={<Logout />} />
                    <Route path="/login" element={<Login />} />
                </Routes>
            </div>
        </Router>
    );
}

export default App;
