import { createContext, useState, useEffect } from 'react';
import api from './Api';

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
    const [user, setUser] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchUser = async () => {
            try {
                const response = await api.get('/user', { withCredentials: true });
                setUser(response.data.user);
            } catch (error) {}
            setLoading(false);
        };

        fetchUser();
    }, []);

    const register = async (data) => {
        const response = await api.post('/register', data);
        setUser(response.data.user);
        return response;
    };

    const login = async (data) => {
        const response = await api.post('/login', data);
        setUser(response.data.user);
        return response;
    };

    const logout = async () => {
        await api.post('/logout');
        setUser(null);
    };

    return (
        <AuthContext.Provider value={{ user, loading, register, login, logout }}>
            {children}
        </AuthContext.Provider>
    );
};
