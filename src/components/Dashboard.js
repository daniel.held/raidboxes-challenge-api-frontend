import {useContext, useEffect, useState} from 'react'
import MessageFeed from "./MessageFeed";
import CreateMessage from "./CreateMessage";
import {AuthContext} from "../AuthContext";
import Header from "./Header";
import {useNavigate, useParams} from "react-router-dom";
import UserCard from "./UserCard";
import EditMessage from "./EditMessage";
import api from "../Api";

export default function Dashboard(props) {
    const [messages, setMessages] = useState([]);
    const [reload, setReload] = useState(false);
    const [userFilter, setUserFilter] = useState([]);
    const [tagFilter, setTagFilter] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const {method, id} = useParams();
    const navigate = useNavigate();
    const {user} = useContext(AuthContext);

    function handleReload() {
        setReload(!reload);
    }

    function filterByUser(userID) {
        setUserFilter([userID]);
    }

    function filterByTag(tagID) {
        setTagFilter([tagID]);
    }

    useEffect(() => {
        setIsLoading(true);

        async function fetchMessages() {
            let tagFilterValue = tagFilter;
            let userFilterValue = userFilter;
            if (method === undefined && id === undefined) {
                tagFilterValue = [];
                userFilterValue = [];
            }
            if (method === "user") {
                tagFilterValue = [];
                setTagFilter([]);
            }
            if (method === "tags") {
                userFilterValue = [];
                setUserFilter([]);
            }
            const filters = {
                users: userFilter.length <= 0 && method === "user" ? [id] : userFilterValue,
                tags: tagFilter.length <= 0 && method === "tags" ? [id] : tagFilterValue,
            };
            const usersArrayParamString = filters.users.join(',');
            const tagsArrayParamString = filters.tags.join(',');
            const response = await api.get('/messages',
                {
                    params: {
                        users: usersArrayParamString,
                        tags: tagsArrayParamString,
                    },
                    withCredentials: true
                });
            let messageData = response.data;
            setMessages(messageData);
            if (method !== undefined && messageData.length === 0) {
                navigate("../");
            }
            setIsLoading(false);
        }

        fetchMessages();
    }, [reload, method, id]);

    return (
        <>
            <div className="min-h-full">
                <Header/>
                <div className="py-10">
                    <main className="mt-10">
                        <div className="mx-auto max-w-lg px-6">
                            {isLoading && !user && <p>Loading...</p>}
                            {!isLoading && (
                                <>
                                    {user && !method && <CreateMessage handleReload={handleReload} reload={reload}/>}
                                    {method === "user" && <UserCard/>}
                                    {method !== "edit" && <MessageFeed
                                        messages={messages}
                                        handleReload={handleReload}
                                        filterByUser={filterByUser}
                                        filterByTag={filterByTag}
                                    />}
                                    {method === "edit" && <EditMessage handleReload={handleReload} reload={reload}/>}
                                </>
                            )}
                        </div>

                    </main>
                </div>
            </div>
        </>
    )
}
