import {useState} from "react";
import api from "../Api";

export default function DeleteButton({messageID, handleReload}) {
    const [errors, setErrors] = useState([]);

    async function deleteMessage() {
        try {
            await api.delete(
                '/messages',
                {
                    data: { message_id: messageID },
                    withCredentials: true,
                }
            );

            handleReload();
        } catch (error) {
            if (error.response && error.response.status === 401) {
                setErrors(error.response.data.errors);
            }
        }
    }

    return(
        <button onClick={deleteMessage} style={{width: "100%"}} className="block px-4 py-2 hover:bg-gray-200">Löschen</button>
    )
}
