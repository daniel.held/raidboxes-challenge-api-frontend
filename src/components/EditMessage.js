import TextareaAutosize from "react-textarea-autosize";
import TagSelect from "./TagSelect";
import {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import api from "../Api";


export default function EditMessage({handleReload, reload}) {
    const [selectedTag, setSelectedTag] = useState("");
    const [content, setContent] = useState("");
    const [isLoading, setIsLoading] = useState(true);
    const [errors, setErrors] = useState(null);
    const { id } = useParams();
    const navigate = useNavigate();

    function setSelectedTagHandle(name) {
        setSelectedTag(name)
    }

    async function handleSubmit(e) {
        e.preventDefault();
        try {
            await api.put(
                '/messages',
                { message_id: id, tag: selectedTag, content },
                { withCredentials: true }
            );
            setErrors([]);
            setSelectedTag("");
            setContent("");
            navigate("../")
        } catch (error) {
            if (error.response && error.response.status === 401) {
                setErrors(error.response.data.errors);
            }
        }
    }

    useEffect(() => {
        api.get('/messages',
            {
                params: {
                    id: id,
                },
                withCredentials: true
            }).then((r) => {
                const message = r.data[0];
                setContent(message.content);
                setSelectedTag(message.tag_name);
                setIsLoading(false);
        });
    }, [reload])

    return(
        <>
            {!isLoading && <form onSubmit={handleSubmit} className="relative">
                <div className="overflow-hidden rounded-lg border border-gray-300 shadow-sm focus-within:border-indigo-500 focus-within:ring-1 focus-within:ring-indigo-500">
                    <label htmlFor="description" className="sr-only">
                        Message
                    </label>
                    <TextareaAutosize
                        minRows={2}
                        name="description"
                        id="description"
                        maxLength={240}
                        value={content}
                        onChange={(e) => {
                            const newValue = e.target.value.replace(/[\r\n]+/g, '');
                            setContent(newValue);
                        }}
                        className="pt-2 block w-full resize-none border-0 py-0 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                        placeholder="Write a description..."
                    />
                    <div className=" bg-white text-right px-2 py-1 text-gray-400 text-s">{content.length} / 240</div>

                    <div aria-hidden="true">
                        <div className="py-2">
                            <div className="h-9" />
                        </div>
                    </div>
                </div>


                <div className="absolute inset-x-px bottom-0">
                    <div className="flex items-center justify-between space-x-3 border-t border-gray-200 px-2 py-2 sm:px-3">
                        <div className="flex">
                            <TagSelect setSelectedTagHandle={setSelectedTagHandle} reload={reload} predefinedSelectedTag={selectedTag}/>
                        </div>
                        <div className="flex-shrink-0">
                            <button
                                type="submit"
                                className="inline-flex items-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                            >
                                Update
                            </button>
                        </div>
                    </div>
                </div>
            </form>}
        </>
    );
}
