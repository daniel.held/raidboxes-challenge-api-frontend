import { useEffect } from 'react';
import {useNavigate} from "react-router-dom";
import api from "../Api";

function Logout() {
    const navigate = useNavigate();
    useEffect(() => {
        const logout = async () => {
            try {
                await api.post('/logout', {}, { withCredentials: true });
                navigate("/");
                window.location.reload();

            } catch (error) {
                console.error('Error logging out:', error);
            }
        };

        logout();
    }, []);

    return (
        <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
            <div className="max-w-md w-full space-y-8">
                <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Logging out...</h2>
            </div>
        </div>
    );
}

export default Logout;
