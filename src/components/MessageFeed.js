import {useContext, useEffect} from "react";
import {AuthContext} from "../AuthContext";
import DeleteButton from "./DeleteButton";
import {useNavigate, Link} from "react-router-dom";

export default function MessageFeed({messages, reload, handleReload, filterByUser, filterByTag}) {
    const {user} = useContext(AuthContext);
    const navigate = useNavigate();

    function toggleDropdown(event, id) {
        event.stopPropagation();
        const dropdownMenu = document.getElementById('dropdown-menu-' + id);
        dropdownMenu.classList.toggle('hidden');
    }

    useEffect(() => {

    }, [reload])

    return (
        <div className="flow-root">
            {
                messages.map((message) => (
                    <div key={message.id} className="bg-white shadow-md rounded-md p-4 mb-4 relative mt-4">
                        <div className="flex justify-between items-center mb-4">
                            <div className="flex items-center hover:cursor-pointer" onClick={() => {
                                filterByUser(message.user_id)
                                navigate("../user/" + message.user_id)
                            }}>
                                <span className="font-bold">{message.user_name}</span>
                            </div>
                            {user && user.id === message.user_id && (
                                <div className="relative">
                                    <button className="bg-gray-200 rounded-md py-1 px-2 focus:outline-none"
                                            onClick={(event) => toggleDropdown(event, message.id)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 text-gray-700"
                                             viewBox="0 0 20 20" fill="currentColor">
                                            <path fillRule="evenodd"
                                                  d="M2 4a2 2 0 012-2h12a2 2 0 012 2v1a2 2 0 01-2 2H4a2 2 0 01-2-2V4zm2-1a1 1 0 00-1 1v1h14V4a1 1 0 00-1-1H4zm-1 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm1 5a1 1 0 000 2h8a1 1 0 000-2H4z"
                                                  clipRule="evenodd"/>
                                        </svg>
                                    </button>
                                    <div id={`dropdown-menu-${message.id}`}
                                         className="bg-gray-100 hidden absolute right-0 top-full mt-2 shadow-md rounded-md py-2 z-10">
                                        <Link to={'../edit/' + message.id} className="block px-4 py-2 hover:bg-gray-200">Bearbeiten</Link>
                                        <DeleteButton messageID={message.id} handleReload={handleReload}/>
                                    </div>
                                </div>
                            )}
                        </div>
                        <p className="text-gray-800 mb-2 break-words">{message.content}</p>
                        <div className="flex justify-between items-center">
                            <span className="text-blue-800 text-sm hover:cursor-pointer" onClick={() => {
                                filterByTag(message.tag_id)
                                navigate("../tags/" + message.tag_id)
                            }}>#{message.tag_name}</span>
                            <span className="text-gray-500 text-sm">{message.created_at}</span>
                        </div>
                    </div>
                ))
            }
        </div>
    );
}
