import { useEffect, useState } from 'react';
import { CheckIcon, ChevronUpDownIcon } from '@heroicons/react/20/solid';
import { Combobox } from '@headlessui/react';
import api from "../Api";

function classNames(...classes) {
    return classes.filter(Boolean).join(' ');
}

export default function TagSelect({setSelectedTagHandle, predefinedSelectedTag, reload}) {
    const [query, setQuery] = useState('');
    const [selectedTag, setSelectedTag] = useState("");
    const [tags, setTags] = useState([]);

    useEffect(() => {
        setSelectedTag("");
        const fetchTags = async () => {
            try {
                const response = await api.get('/tags', { withCredentials: true });
                setTags(response.data.data);
                setSelectedTag({ name: predefinedSelectedTag });
            } catch (error) {}
        };

        fetchTags();
    }, [reload]);

    const filteredTags =
        query === ''
            ? tags
            : tags.filter((tag) => {
                return tag.name.toLowerCase().includes(query.toLowerCase());
            });

    const handleInputChange = (event) => {
        setQuery(event.target.value.toLowerCase());
        setSelectedTag({ name: event.target.value.toLowerCase() });
        setSelectedTagHandle(event.target.value.toLowerCase());
    };

    function handlesetSelectedTag(event) {
        setSelectedTag(event);
        setSelectedTagHandle(event.name.toLowerCase());
    }

    return (
        <Combobox as="div" value={selectedTag} onChange={(event) => handlesetSelectedTag(event)}>
            <div className="relative">
                <Combobox.Input
                    className="w-full rounded-md border-0 bg-white py-1.5 pl-3 pr-10 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                    onChange={handleInputChange}
                    displayValue={(tag) => tag?.name}
                    placeholder={"#tag"}
                />
                <Combobox.Button className="absolute inset-y-0 right-0 flex items-center rounded-r-md px-2 focus:outline-none">
                    <ChevronUpDownIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
                </Combobox.Button>

                {filteredTags.length > 0 && (
                    <Combobox.Options className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                        {filteredTags.map((tag) => (
                            <Combobox.Option
                                key={tag.id}
                                value={tag}
                                className={({ active }) =>
                                    classNames(
                                        'relative cursor-default select-none py-2 pl-3 pr-9',
                                        active ? 'bg-indigo-600 text-white' : 'text-gray-900'
                                    )
                                }
                            >
                                {({ active, selected }) => (
                                    <>
                                        <span className={classNames('block truncate', selected && 'font-semibold')}>{tag.name}</span>

                                        {selected && (
                                            <span
                                                className={classNames(
                                                    'absolute inset-y-0 right-0 flex items-center pr-4',
                                                    active ? 'text-white' : 'text-indigo-600'
                                                )}
                                            >
                                                <CheckIcon className="h-5 w-5" aria-hidden="true" />
                                            </span>
                                        )}
                                    </>
                                )}
                            </Combobox.Option>
                        ))}
                    </Combobox.Options>
                )}
            </div>
        </Combobox>
    );
}
