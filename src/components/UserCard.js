import {useContext, useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {AuthContext} from "../AuthContext";
import api from "../Api";

export default function UserCard() {
    const {id} = useParams();
    const {user} = useContext(AuthContext);
    const [isFollowing, setIsFollowing] = useState(false);
    const [showIsFollowing, setShowIsFollowing] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [userInfo, setUserInfo] = useState();

    async function handleFollow() {
        await api.post(`/follow/${id}`, {}, {withCredentials: true});
        setIsFollowing(true);
    }

    async function handleUnfollow() {
        await api.delete(`/unfollow/${id}`, {withCredentials: true});
        setIsFollowing(false);
    }

    async function fetchUserInfo() {
        try {
            const response = await api.get(`/user/${id}/info`);
            setUserInfo(response.data.user);
        } catch (error) {
            console.error("Error fetching user info:", error);
        }
    }

    async function checkFollowingStatus() {
        try {
            const response = await api.get(`/user/`, {withCredentials: true});
            if (user) {
                const userFollowsTarget = response.data.user.following.some(followingUser => {
                    const targetId = parseInt(id);
                    return followingUser.id === targetId;
                });

                setIsFollowing(userFollowsTarget);
                setShowIsFollowing(true);
            }
        } catch (error) {}
    }

    useEffect( () => {
        fetchUserInfo().then(() => {
            checkFollowingStatus().then(() => {
                setIsLoading(false);
            });
        });
    }, [])

    return (
        !isLoading && (
            <div className="border-b border-gray-200 bg-white px-4 py-5 sm:px-6">
                <div className="-ml-4 -mt-2 flex flex-wrap items-center justify-between sm:flex-nowrap">
                    <div className="ml-4 mt-2">
                        <h3 className="text-base font-semibold leading-6 text-gray-900">{userInfo.name}</h3>
                    </div>
                    {showIsFollowing && <div className="ml-4 mt-2 flex-shrink-0">
                        <button
                            onClick={isFollowing ? handleUnfollow : handleFollow}
                            type="button"
                            className="relative inline-flex items-center rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                        >
                            {isFollowing ? "Unfollow" : "Follow"}
                        </button>
                    </div>}
                </div>
            </div>
        )
    );
}
